<?php

    require("src/bdd.php");

    if (!empty($_POST['pseudo']) and !empty($_POST['password'])) {

        // RECUPERATION DES VALEURS DANS UNE VARIABLE
        $pseudo = htmlspecialchars($_POST['pseudo']);
        $password = htmlspecialchars($_POST['password']);

        // AUTHENTIFICATION DES DONNEES : HACHAGE MD5 ET SHA1 PAR UN GRAIN DE SEL ($salt)
        $password = sha1(md5($salt.md5(sha1($salt.$password.$salt)).$salt));

        // VERIFICATION SUR LE NOMBRE DE USER CORRESPONDANT AUX ENTREES
        $userBD = $newBD->prepare('SELECT COUNT(*) AS nbrUser FROM `user` WHERE `pseudo` = ? AND `password` = ?');
        $userBD->execute(array($pseudo, $password)) or die(print_r($newBD->errorInfo()));
        $nbrUserCount = $userBD->fetch();

        // VERIFICATION SUR L'EXISTANCE DE L'UTILISATEUR
        if ($nbrUserCount['nbrUser'] == 1) {
            header("Location: ?succes=1");
            exit();
        }else {
            header("Location: ?error=1");
            exit();
        }

    }

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- ========== Meta ================================================== -->
        <meta charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <!-- Title of the page / Titre de la page -->
        <title>Login</title>
        <!-- Icon next to the page title / Icon à côté du titre de la page -->
        <link rel="short icon" type="text/css" href="assets/img/favico.png"/>
        <!-- Css link / Lien css -->
        <link rel="stylesheet" href="assets/css/style.css"/> 
    </head>
    <body>
        <!-- Login / Connexion -->
        <div class="login-box log-box-c">
            <img src="assets/img/avatar.png" class="avatar"/>
            <legend> <h1> Login </h1> </legend>
            <form method="POST" name="" action="">
                <input type="text" name="pseudo" placeholder="Enter your pseudo" required=""/>
                <input type="password" name="password" placeholder="Enter your password" required=""/>
                <input type="submit" name="ok" value="Validate"/>
                <input type="reset" name="ok" value="Cancel"/>
                <a href="register.php">Create an account !</a>
            </form>
        </div>
    </body>
</html>