<?php

    require("src/bdd.php");

    if (!empty($_POST['pseudo']) and !empty($_POST['password']) and !empty($_POST['renew_password'])) {
        
        // RECUPERATION DES VALEURS DANS UNE VARIABLE
        $pseudo = htmlspecialchars($_POST['pseudo']);
        $password = htmlspecialchars($_POST['password']);
        $renew_password = htmlspecialchars($_POST['renew_password']);

        // VERIFICATION SUR LE NOMBRE DE PSEUDO CORRESPONDANT A L'ENTREE
        $pseudoBD = $newBD->prepare('SELECT COUNT(*) AS nbrPseudo FROM `user` WHERE `pseudo` = ?');
        $pseudoBD->execute(array($pseudo)) or die(print_r($newBD->errorInfo()));
        $nbrPseudoCount = $pseudoBD->fetch();

        // VERIFICATION SUR L'EXISTANCE DU PSEUDO
        if ($nbrEmailCount['nbrPseudo'] == 0) {

            // VERIFICATION SUR LA CORRESPONDANCE DU MOT DE PASSE
            if ($password == $renew_password) {

                // INTEGRITE DES DONNEES : HACHAGE MD5 ET SHA1 PAR UN GRAIN DE SEL ($salt)
                $password = sha1(md5($salt.md5(sha1($salt.$password.$salt)).$salt));

                // INSERTION EN BASE DE DONNEES
                $insert = $newBD->prepare('INSERT INTO `user`(`pseudo`, `password`) VALUES(?,?)');
                $insert->execute(array($pseudo, $password)) or die(print_r($newBDD->errorInfo()));
                header("Location: ?succes=1");
                exit();
            }else {
                header("Location: ?error=1");
                exit();
            }

        }else {
            header("Location: ?error=1");
            exit();
        }

    }

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- ========== Meta ================================================== -->
        <meta charset="UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <!-- Title of the page / Titre de la page -->
        <title>Register</title>
        <!-- Icon next to the page title / Icon à côté du titre de la page -->
        <link rel="short icon" type="text/css" href="assets/img/favico.png"/>
        <!-- Css link / Lien css -->
        <link rel="stylesheet" href="assets/css/style.css"/> 
    </head>
    <body>
        <!-- Login / Connexion -->
        <div class="login-box log-box-r">
            <img src="assets/img/avatar.png" class="avatar"/>
            <legend> <h1> Register </h1> </legend>
            <form method="POST" name="" action="">
                <input type="text" name="pseudo" placeholder="Enter your pseudo" required=""/>
                <input type="password" name="password" placeholder="Enter your password" required=""/>
                <input type="password" name="renew_password" placeholder="Confirm Password" required=""/>
                <input type="submit" name="ok" value="Validate"/>
                <input type="reset" name="ok" value="Cancel"/>
                <a href="index.php">Login !</a>
            </form>
        </div>
    </body>
</html>